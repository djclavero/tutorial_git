# Git Basics

## Basic commands

### git status
```
# Checking the status of your files
git status -s
git status
```

*It shows modified and untracked files to be staged:*

![Picture](Pics/modified.PNG)

### git add
```
# Stage file
git add <file>

# Stage all files
git add *

# Checking again
git status
```

*It shows staged files to be committed:*

![Picture](Pics/staged.PNG)

### git commit
```
# Create snapshot of the current Project
# git commit -m <description>

git commit -m "Added some changes and new files"
```
*Commit message:*

![Picture](Pics/committed.PNG)

```
# Checking again
git status
```
*It shows no files to be committed:*

![Picture](Pics/working_tree_clean.PNG)


## Ignoring files
Create **.gitignore** file in the root of the Project.

Edit .gitignore and set the patterns of files and directories to be ignored by Git:

```
# ignore by extension file
# *.pyc

# ignore by directory
# /directory

/DocuTech
```

## Useful commands
### git diff
```
# Shows unstaged changes
git diff 

# Compares staged changes to the last commit
git diff --staged 
```
### git rm
```
# Remove file from Git 
git rm <file>

# Remove all files from Git
git rm <Directory>/*
```

## Undoing things
- Unstage a staged file
```
git reset HEAD <file>
```

- Unmodify a modified file (revert it back to last commit)
```
git checkout -- <file>
```
- If you commit and forget add a file to this commit 
```
git commit -m "some commit"
git add <forgotten_file>
git commit --amend   # replace last commit
```

