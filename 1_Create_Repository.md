# Create Repository

Two ways to obtain a repository:
- Clone repository
- Create local repository

>Clone repository (**git clone**)

```
# Clone existing repository (p.e. tutorial-biopython from Bitbucket)
git clone https://djclavero@bitbucket.org/djclavero/tutorial_biopython.git
```

>Create local repository (**git init**)

```
# Go inside your Project folder (p.e. ./Tutorial-Git)
git init

# Checking a hidden directory .git was created
dir /A
```

Public/Private repositories can be created in Bitbucket (p.e. tutorial-git)

Then you can get your local repository in Bitbucket:

1. Go to your local repository's directory (p.e. ./Tutorial-Git)

2. Connect your existing repository to Bitbucket

```
# Add remote repository
git remote add origin 'https://djclavero@bitbucket.org/djclavero/tutorial_git.git'

# Checking
git remote -v

# Upload to remote repository
git push -u origin master

# If created a GitHub repository, rename branch from 'main' to 'master'
```


Notes:

- Delete the local repository by deleting the Project directory

- Delete the remote repository by deleting the Repository in Bitbucket



