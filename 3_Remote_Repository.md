# Remote Repository

### Adding a remote repository
```
git remote add <shortname> <url>
```

### Showing your remote repositories
```
git remote -v

origin  https://djclavero@bitbucket.org/djclavero/tutorial_git.git (fetch)
origin  https://djclavero@bitbucket.org/djclavero/tutorial_git.git (push)
```

### Download data from remote repository
*git fetch command only downloads the data to your local repository, but does not make 'merge'*
```
git fetch origin
```

### Upload data to repository repository
```
# Push your 'master' branch to your 'origin' server
git push origin master

Enumerating objects: 55, done.
Counting objects: 100% (55/55), done.
Delta compression using up to 4 threads
Compressing objects: 100% (48/48), done.
Writing objects: 100% (50/50), 97.34 KiB | 4.06 MiB/s, done.
Total 50 (delta 17), reused 0 (delta 0)
To https://bitbucket.org/djclavero/tutorial_git.git
   8883466..1aee91d  master -> master

# Checking
git status

On branch master
Your branch is up to date with 'origin/master'.
nothing to commit, working tree clean
```

### Show information about your remote repository
```
git remote show origin

* remote origin
  Fetch URL: https://djclavero@bitbucket.org/djclavero/tutorial_git.git
  Push  URL: https://djclavero@bitbucket.org/djclavero/tutorial_git.git
  HEAD branch: master
  Remote branch:
    master tracked
  Local branch configured for 'git pull':
    master merges with remote master
  Local ref configured for 'git push':
    master pushes to master (fast-forwardable)
```

### Other commands
```
# Rename shortname of remote repository
git remote <old_shortname> <new_shortname>

# Checking
git remote

# Remove remote repository
git remote remove <shortname>
```









