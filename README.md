# Git Tutorial
This is a Tutorial about Git, a free Distributed Version Control System.

https://git-scm.com/

## VCS

Version Control System (VCS) is a system that records changes to a set of files over time so you can recall a specific version later.

VCS types:

* Local 
    - Database in the local computer keeping changes to files 

* Centralized (CVCS)
    - Database located in a Server that contains all versions of the files and provides access to clients
    - p.e. CVS, Subersion, etc.

* Distributed (DVCS)
    - Clients mirror the repository, including its full history
    - p.e. Git, Mercury, Bazaar, etc.


## Git Philosophy
Git thinks about its data like a stream of "snapshots".

*States* of tracked files:

- Modified

- Staged

- Committed

![Picture](Pics/States.PNG)

# Configuration

### Version

```
# Check Git version
git --version    

# Help
git help 
git help <command>
```

### Configure your identity

```
# Using --global the settings will be available for all repositories
git config --global user.name 'djclavero'
git config --global user.email djclavero@yahoo.com

# Checking
git config user.name
git config user.email
```

### Configure your text editor

```
# Visual Studio Code like text editor
git config --global core.editor "code --wait"

# Checking
git config core.editor

# Open configuration using the editor
git config -e --global
```

### Show Configuration

```
git config --list

git config --list --global    # all repositories settings
git config --list --system    # system settings
git config --list --local     # current repository settings
```
