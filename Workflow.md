# Git Workflow

## Project 
>Project folder

* Working tree (Project files)

* .git directory

    - Staging Area

    - Local Repository

## Files
>States

* Untracked (**U**)

* Tracked

    - Modified (**M**)

    - Staged (**A**)

    - Committed

```
# Check states of files
git status
```

## Workflow

![Picture](Pics/Workflow.PNG)


